public with sharing class TextHandler {
	public final static String NEW_LINE = '\n';

	public String findDuplicateCharacter(final String input) {
		String output = '';
		if (String.isNotBlank(input) && input.contains(NEW_LINE) == false) {
			List<String> lines = input.split(NEW_LINE);

			for (String line : lines) {
				final Set<String> charSet = new Set<String>(line.split(''));
				if (line.length() > charSet.size()) {
					output += String.format('{0}{1}', new Object[]{
							line, NEW_LINE
					});
				}
			}
		}

		return output;
	}
}
